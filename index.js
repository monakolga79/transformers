/**
 * Задание 5 - Трансформеры и ООП
 * 
 * 1. Создать класс Transformer со свойствами name и health (по умолчанию
 *    имеет значение 100) и методом attack()
 * 2. Создать класс Autobot, который наследуется от класса Transformer.
 *    - Имеет свойсто weapon, т.к. автоботы сражаются с использованием оружия.
 *    - Конструктор класса принимает 2 параметра: имя и оружее (Экземпляр класса
 *      Weapon). 
 *    - Метод attack возвращает результат использования оружия weapon.fight()
 * 3. Создать класс Deceptikon, который наследуется от класса Transformer.
 *    - Десептиконы не пользуются оружием, поэтому у них нет свойства weapon. Зато
 *      они могут иметь разное количество здоровья.
 *    - Конструктор класса принимает 2 параметра: имя и количество здоровья health
 *    - Метод attack возвращает характеристики стандартного вооружения: { damage: 5, speed: 1000 }
 * 4. Создать класс оружия Weapon, на вход принимает 2 параметра: damage - урон 
 *    и speed - скорость атаки. Имеет 1 метод fight, который возвразает характеристики 
 *    оружия в виде { damage: 5, speed: 1000 }
 * 5. Создать 1 автобота с именем OptimusPrime с оружием, имеющим характеристики { damage: 100, speed: 1000 }
 * 6. Создать 1 десептикона с именем Megatron и показателем здоровья 10000
 * 7. Посмотреть что происходит при вызове метода atack() у траснформеров разного типа, 
 *    посмотреть сигнатуры классов
 * 
 * 8. ДЗ-Вопрос: Написаиь симуляцию. Сколько нужно автоботов чтобы победить Мегатрона если параметр speed в оружии это
 *    количество милсекунд до следующего удара?
 */

class Weapon {
  _damage;
  _speed;
  constructor(damage = 5, speed = 1000) {
    this._damage = damage;
    this._speed = speed;
  }
  fight() {
    return { damage: this._damage, speed: this._speed };
  }
}

class Transformer {
  _name;
  _health;

  constructor(name, health) {
    this._name = name;
    this._health = health;
  }

  attack() {}

  hit(weapon) {
    this._health = this._health - weapon.damage;
  }
}

class Autobot extends Transformer {
  _weapon;

  constructor(name, weapon, health) {
    super(name, health);

    this._weapon = weapon;
  }

  attack() {
    return this._weapon.fight();
  }
}

class Deceptikon extends Transformer {
  _weapon = { _damage: 20,  _speed: 1000 };

  attack() {
    return { 
      damage: this._weapon._damage, 
      speed: this._weapon._speed 
    };
  }
}




class Arena {
  constructor(side1, side2) {
    this.side1 = side1;
    this.side2 = side2;
  }

  start() {
    const timer = setInterval(() => {
      this.side1.forEach(bot => this.side2[0].hit(bot.attack()));
      this.side2 = this.side2.filter(bot => bot._health > 0);
      this.fn(this.side1, this.side2); // обновление состояния арены
      
      if (!this.side2.length) {
        clearInterval(timer);
        console.log('Сторона 1 победила!')
        return;
      }
  
      this.side2.forEach(bot => this.side1[0].hit(bot.attack()));
      this.side1 = this.side1.filter(bot => bot._health > 0);
      this.fn(this.side1, this.side2); // обновление состояния арены

      if (!this.side1.length) {
        clearInterval(timer);
        console.log('Сторона 2 победила!')
        return;
      }
    }, 1000);
  }

  // функция-наблюдатель для вызова Visualizer для обновления состояния арены
  subscribe(fn){
    this.fn = fn;
    this.fn(this.side1, this.side2); // инициализация состояния арены
  }
}


const arena = new Arena(
  [ 
    new Autobot('Optimus Prime', new Weapon(), 100),
    new Autobot('Soullessbot1', new Weapon(), 100),
    new Autobot('Soullessbot2', new Weapon(), 100)
  ], 
  [ 
    new Deceptikon('Megatron', 10000)
  ]
);


class Visualizer {

  // статическая функция для внутреннего вызова, для очистки арены и добавления текущего числа ботов
  static render(side1, side2) {
    const side1El = document.querySelector('.arena-side-1');
    const side2El = document.querySelector('.arena-side-2');

    side1El.innerHTML = '';
    side2El.innerHTML = '';

    side1.forEach(bot => side1El.append(Visualizer.getBotNode(bot)));
    side2.forEach(bot => side2El.append(Visualizer.getBotNode(bot)));
  }
  // статическая функция для внутреннего вызова, для создания в DOM дереве узла для отображения бота
  static getBotNode(bot) {
    const div = document.createElement('div'); // <div></div>
    div.classList.add('bot');                  // <div class="bot"></div>

    const span = document.createElement('span'); // <span></span>
    span.textContent = bot._health + 'HP';       // <span>100 HP</span>

    div.append(span);                          // <div class="bot"><span>100 HP</span></div>

    return div;  // <div class="bot"><span>100 HP</span></div>
  }
}

arena.start();
arena.subscribe(Visualizer.render);